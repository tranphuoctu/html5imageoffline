<?php

$imgPath=  dirname(__FILE__) . '/images';
$imageFiles=  scandir($imgPath);

$results=array();
$index=1;
foreach($imageFiles as $file){
    if($file!='.' && $file!='..'){
        $results[]=array(
            'id'=>$index,
            'name'=>$file,
            'url'=>"images/$file"
        );
        $index++;
    }
}

header('Content-Type: application/json');
echo json_encode($results);
?>