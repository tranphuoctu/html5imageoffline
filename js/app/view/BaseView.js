var BaseView = Backbone.View.extend({
    title: '',
    viewParam:{},
    
    initialize: function () {
        this.render();
    },
    render: function () {
        var that = this;
        $('#title').html(this.getTitle());
        this.beforeTemplate(function(){            
            var template = _.template(that.template);
            $contents.empty().append(that.$el.html(template(this.viewParam)));
                        
            this.afterTemplate();
            $('#main-app').trigger('create');
            
        }.bind(this));
        
        return this;
    },
    beforeTemplate: function (next) {
        next();
    },
    
    afterTemplate: function () {
    },
    addViewParam: function(key, value){
        this.viewParam[key]=value;
    },
    
    changeView: function(route){
        app.router.navigate(route, {trigger: true});
    },
    
    getTitle: function(){
        return this.title;
    }
});