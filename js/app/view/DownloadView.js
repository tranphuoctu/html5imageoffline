var DownloadView=BaseView.extend({
    title: 'Donwload Image',
    template: $('#downloading').html(),
    
    beforeTemplate: function(next){
        var self=this;
        service.checkConnectApi()
            .then(function(success){
                if(!success){
                    alert('Connect server fail');
                    self.changeView('/');
                }else{
                    next();
                }
            });
    },
    
    afterTemplate: function(){
        var self=this;
        
        service.getAndSaveImages()
            .then(function(){
                app.log.info('Done');
                self.changeView('/');
            });
    }
});
