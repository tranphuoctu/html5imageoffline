var TopView=BaseView.extend({
    title: 'Gallery',
    template: $('#top').html(),
    
    imageCollection: null,
    
    beforeTemplate: function(next){        
        var imageCollection=new ImageCollection();
        var self=this;
        imageCollection.findAllData()
            .then(function(models){
                self.addViewParam('images', models);
                self.imageCollection=imageCollection;
                next();
            });
    },
    
    afterTemplate: function(){
        this.$el.find('#gallery').flexImages({
            rowHeight: 140
        });       
        this.$el.find('#gallery').find("a.grouped_elements").fancybox({
            overlayOpacity: 0.6
        });      
        
        var self=this;
        _.each(this.imageCollection.models, function(imageModel){
            service.readImage(imageModel.get('id'))
                .then(function(imageData){
                    var dom=self.$el.find('#img-'+imageModel.get('id'));
                    dom.attr('src', imageData);
                    dom.parent().attr('href', imageData);
                });
        });
    }
});

