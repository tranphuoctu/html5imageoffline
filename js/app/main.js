var app = app || {};

app.debug=true;

app.database={
    id:"html5-offline-image",
    description:"Html5 Offline Image App",
    nolog: true,
    migrations:[
        {
            version:"1",
            migrate:function (transaction, next) {                
                transaction.db.createObjectStore('image', { keyPath: "id" });
                transaction.db.createObjectStore('image_data', { keyPath: "imgId" });
                       
                var store = transaction.objectStore("image");
                store.createIndex("id", "id", { unique: true});
                
                store=transaction.objectStore("image_data");
                store.createIndex("imgId", "imgId", { unique: true});
                
                next();
            }
        }
    ]
};

app.helper={};
app.helper.bindMethod=function(obj, method){
    return function(){
        return obj[method]();
    };
};  

app.log={};
app.log.info=function(message){
    if(app.debug){
        console.log(message);
    }
};

app.log.error=function(message){
    if(app.debug){
        console.error(message);
    }
};

var $contents = $('#page-content');

$(function(){
    if (typeof indexedDB === "undefined"){
        alert('Browser does not support IndexedDB');
    }else{        
        app.router = new AppRouter();            
        Backbone.history.start();
    }
});

