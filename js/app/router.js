var AppRouter = Backbone.Router.extend({
    routes: {
        download: 'onDownload',
        "*actions": "onTop" 
    },
    
    onTop: function(){
        new TopView();
    },
    
    onDownload: function(){
        new DownloadView();
    }
    
});