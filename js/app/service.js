var service={};

service.checkConnectApi=function(){
    var deferred=$.Deferred();
    var imageCollection=new ImageCollection();
    $.ajax({
        url: imageCollection.tableConfig.apiFetchUrl,
        type:'HEAD',
        async:true,
        success:function(){
            deferred.resolve(true);
        },
        error: function(){
            deferred.resolve(false);
        }
    });
    return deferred.promise();
};

service.getAndSaveImages=function(){
    var deferred=$.Deferred();
    var imageCollection=new ImageCollection();
    var imageDataCollection=new ImageDataCollection();
    
    imageDataCollection.deleteLocalData()        
        .then(app.helper.bindMethod(imageCollection, 'deleteLocalData'))
        .then(app.helper.bindMethod(imageCollection, 'fetchDataFromServer'))
        .then(function(data){            
            imageCollection.add(data);
            return data;
        })
        .then(app.helper.bindMethod(imageCollection, 'saveDataToStorage'))
        .then(function(){
            return util.runMultiPromiseTasks(function(taskIndex){
                    var model=imageCollection.models[taskIndex];
                    if(model){
                        return service.saveImage(model.get('id'), model.get('url'));
                    }else{
                        return true;
                    }
                }, 
                imageCollection.length,
                function(taskIndex){
                    var model=imageCollection.models[taskIndex];
                    if(model){
                        console.error('Can not save image file: '+model.get('url'));
                    }
                });
        })
        .then(function(){
            deferred.resolve();
        });            
            
    return deferred.promise();
};

service.downloadImageByAjax=function(imageUrl){
    var deferred=$.Deferred();
    
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (this.readyState == 4){
            if(this.status == 200){
                deferred.resolve(this.response);
            }else{
                deferred.resolve(null);
            }
        }
    };
    
    xhr.open('GET', imageUrl);
    xhr.responseType = 'blob';
    xhr.send();   
    
    return deferred.promise();
};

service.saveImage= function(imgId, imgUrl){
    var deferred=$.Deferred();

    service.downloadImageByAjax(imgUrl)
            .then(function(fileData){
                if(fileData){
                    return util.convertBlobToBase64(fileData);
                }else{
                    return null;
                }
            })
            .then(function(data){
                if(data){
                    var collection=new ImageDataCollection();
                    var model=new ImageDataModel({
                        imgId:imgId,
                        data:data
                    });
                    collection.add(model);
                    model.save({}, {
                        success: function(){
                            deferred.resolve(true);
                        },
                        error: function(model, response, options){
                            console.log('Save image data ' + model.get('imgId') + ' fail: '+response);
                            deferred.resolve(false);
                        }
                    });
                }else{
                    deferred.resolve(false);
                }

            });

    return deferred.promise();
};

service.readImage=function(imgId){
    var deferred=$.Deferred();
    
    var collection=new ImageDataCollection();
    collection.findByImageId(imgId)
        .then(function(model){
            if(model){
                deferred.resolve(model.get('data'));
            }else{
                deferred.resolve('');
            }
        });
    
    return deferred.promise();
};