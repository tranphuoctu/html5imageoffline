var util=util || {};

/**
 * Run and wait all tasks finish
 * 
 * @param {callback} task take taskIndex as param, task must return a promise
 * @param {int} totalTaskCount
 * @param {callback} ataskErrorCallback called when a task fail, take taskIndex as param
 * @returns {promise}
 */
util.runMultiPromiseTasks=function(task, totalTaskCount, ataskErrorCallback){
    var deferred=$.Deferred();
    
    var taskIndex=0;
    
    if(totalTaskCount===0){
        deferred.resolve();
    }else{
        runTask();
    }

    function runTask(){
        task(taskIndex)
            .then(function(success){
                taskIndex++;
                if(!success){
                    ataskErrorCallback(taskIndex);
                }
                if(taskIndex<totalTaskCount){
                    runTask();
                }else{
                    deferred.resolve();
                }
            });
    }
    
    return deferred.promise();
};

util.convertBlobToBase64=function(blob){
    var deferred=$.Deferred();
    
    var fr = new FileReader();
    fr.onload = function(e) {
        deferred.resolve(e.target.result);
    };
    fr.readAsDataURL(blob);
    
    return deferred.promise();
};
