var ImageCollection=BaseLocalCollection.extend({
    model: ImageModel,
    tableConfig: {
        tableName: 'image',
        apiFetchUrl: 'list_image.php'
    }
});

