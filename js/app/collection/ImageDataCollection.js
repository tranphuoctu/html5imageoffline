var ImageDataCollection=BaseLocalCollection.extend({
    model: ImageDataModel,
    tableConfig: {
        tableName: 'image_data',
        apiFetchUrl: null
    },
    
    findByImageId: function(id){
        var deferred=$.Deferred();
        
        var self=this;
        this.reset();
        this.fetch({
            conditions:{imgId: id},
            success: function(){
                if(self.models.length>0){
                    deferred.resolve(self.models[0]);
                }else{
                    deferred.resolve(null);
                }
            }
        });
        
        return deferred.promise();
    }
});
