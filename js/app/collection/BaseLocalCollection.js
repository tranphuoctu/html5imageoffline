var BaseLocalCollection = Backbone.Collection.extend({
    tableConfig: {
        tableName: null,
        apiFetchUrl: null
    },

    initialize: function() {
        this.database=app.database;
        this.storeName=this.tableConfig.tableName;
    },
    
    deleteLocalData: function() {
        var deferred = $.Deferred();
        var self = this;

        var options={};
        options.success=function(){
            deferred.resolve();
        };

        self.reset();

        self.sync('delete', self, options);
        return deferred.promise();
    },
    
    fetchDataFromServer: function(){
        var self = this;
        
        return $.ajax({
            url: self.tableConfig.apiFetchUrl,
            dataType: 'json',
            error: function() {
                app.log.error('Fetch error: '+self.tableConfig.tableName);
            }
        });
    },
    
    saveDataToStorage: function() {
        var deferred=$.Deferred();
        
        var count = 0;

        if (this.length > 0) {

            var self = this;
            _.each(this.models, function(model, index, list) {
                self.listenToOnce(model, 'sync', function() {
                    count++;
                    if (count === self.models.length) {
                        deferred.resolve(self.tableConfig.tableName);
                    }
                });
                model.save();
            });
        } else {
            deferred.resolve(this.tableConfig.tableName);
        }
        
        return deferred.promise();
    },
    
    findAllData: function(conditions){
        var deferred=$.Deferred();
        
        var self=this;
        conditions=conditions || null;
        this.reset();
        this.fetch({
            conditions:conditions,
            success: function(){
                deferred.resolve(self.models);
            }
        });
        
        return deferred.promise();
    },
    
    findById: function(id){
        var deferred=$.Deferred();
        
        var self=this;
        this.reset();
        this.fetch({
            conditions:{id: id},
            success: function(){
                if(self.models.length>0){
                    deferred.resolve(self.models[0]);
                }else{
                    deferred.resolve(null);
                }
            }
        });
        
        return deferred.promise();
    },
    
    findByIdAndSetAttributes: function(id, attrs){
        var deferred=$.Deferred();
        this.findById(id).then(function(model){
            model.set(attrs);
            deferred.resolve(model);
        });
        return deferred.promise();
    }
});